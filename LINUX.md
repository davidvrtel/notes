## commands

```shell
du -sh folder/  #size of folder summed up and human readable
sudo !!  #run last command executed with as superuser
echo "ls -l" | at midnight  #run command at specific time
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"   #basic rsa key generation
```
```shell
sudo groupadd $group
sudo adduser $user
sudo usermod -aG $group $user
```

## aliases

```shell
alias ll='exa -alhgF'   #requires exa program, better ls
alias yeet='rm -rf'     #yeet
alias gitdo='f(){ git add -A && git commit -m "$@" && git push; unset -f f; }; f' #git "message" command alias

```

## bind mouse buttons to keystrokes or scripts

Install xbindkeys and necessary utilities. Works only on X11 `sudo apt install xbindkeys x11-utils xdotool`. Command `xev -event mouse | grep Button --before-context=1 --after-context=2` catches mouse button context and prints out button keys, for example Logitech MX Vertical codes for side buttons are `button:8` and `button:9`. Switcher `-event keyboard` can be used to detect key codes.

Add default settings to `$HOME/.xbindkeysrc` with `xbindkeys -d > $HOME/.xbindkeysrc`. Edit the file and add the following

```shell
"xdotool key 'Control+Super_L+Left'"
  b:9

"xdotool key 'Control_L+Super_L+Right'"
  b:8

```

Configuration should get picked up immediately. `xdotool` executes keystrokes which are bound to do stuff in GUI. To debug the configuration, first `killall xbindkeys` and start with `xbindkeys -n -v`. Output will show possible issues with key codes not found and other.


## hotkeys

```
Ctrl + A  Go to the beginning of the line you are currently typing on
Ctrl + E  Go to the end of the line you are currently typing on
Ctrl + L  Clears the Screen, similar to the clear command
Ctrl + U  Clears the line before the cursor position. If you are at the end of the line, clears the entire line.
Ctrl + K  Clears the line after the cursor
Ctrl + W  Delete the word before the cursor
Ctrl + H  #Same as backspace
Ctrl + R  #Let’s you search through previously used commands
Ctrl + C  #Kill whatever you are running
Ctrl + D  Exit the current shell
Ctrl + Z  Puts whatever you are running into a suspended background process. fg restores it.
Ctrl + T  Swap the last two characters before the cursor
Esc + T   Swap the last two words before the cursor
Alt + F   Move cursor forward one word on the current line
Alt + B   Move cursor backward one word on the current line
Tab       #Auto-complete files and folder names
```

## links

> https://wiki.manjaro.org/Linux_Security (manjaro permissions, users, firewall)</br> > https://wiki.archlinux.org/index.php/Security (arch advanced security)
