
#### aliases ####
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias ll='exa -alhgF'
alias buiser='npm run build && clear && serve -s dist/'
alias ssh-caj='ssh root@147.32.110.102'
alias ssh-derg='ssh root@derg.cz'
alias ls='ls --color=auto'
alias gitdo='f(){ git add -A && git commit -m "$@" && git push; unset -f f; }; f' #git "message" command alias
alias yeet='rm -rf'     #yeet

#### prompt formatting ####
txtcyn='\[\e[0;96m\]'
txtyel='\[\e[0;33m\]'
txtwhi='\[\e[37m\]'
txtblu='\[\e[34m\]'
txtcya='\[\033[;94m\]'
txtred='\[\033[1;31m\]'

bold=$(tput bold)
normal=$(tput sgr0)

function gitBranch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
## prompt inspired by kali container
export PS1="\n${txtcya}┌──(${txtred}${bold}\u${normal}${txtcya})──[${txtwhi}\w${txtcya}] ${txtcyn}\$(gitBranch)${txtwhi}\n${txtcya}└─⭘ ${txtwhi}"

