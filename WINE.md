# How to Wine

Everything can be done with `winetricks` and `wine`, some applications need alterations. 

For example Gameranger needs to send raw packets, can be enabled either through `dpkg-reconfigure wine-stable` or setting capability with `sudo setcap cap_net_raw+epi /usr/bin/wine-stable`.

## W3 with gameranger

Get installers and install. If different version is needed it can be either installed or simply copied to the directory.
```bash

 WINEPREFIX=/home/dvrtel/Procrastination/warcraft3 WINEARCH=win64 winetricks --force vcrun2019
 WINEPREFIX=/home/dvrtel/Procrastination/warcraft3 WINEARCH=win64 winetricks --force mfc40
 WINEPREFIX=/home/dvrtel/Procrastination/warcraft3 WINEARCH=win64 winetricks --force mfc42
 WINEPREFIX=/home/dvrtel/Procrastination/warcraft3 WINEARCH=win64 winetricks --force vcrun2005
 WINEPREFIX=/home/dvrtel/Procrastination/warcraft3 WINEARCH=win64 wine ~/Downloads/Wc3_1.21b_ROC\ _enGB/Installer.exe 
 WINEPREFIX=/home/dvrtel/Procrastination/warcraft3 WINEARCH=win64 wine ~/Downloads/GameRangerSetup.exe

 ```
